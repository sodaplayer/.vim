let g:Tex_SmartKeyQuote = 0
set grepprg=egrep\ -nH\ $a
let g:tex_flavor = "latex"
let g:syntastic_mode_map = { 'mode': 'active',
                               \ 'active_filetypes': [],
                               \ 'passive_filetypes': ['java'] }


"Vundle
filetype off

set runtimepath+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'

"Github Repos
Bundle 'scrooloose/nerdtree'
Bundle 'scrooloose/nerdcommenter'
Bundle 'scrooloose/syntastic'
Bundle 'ervandew/supertab'
Bundle 'bitc/vim-hdevtools'
Bundle 'lukerandall/haskellmode-vim'
"Bundle 'Lokaltog/powerline'
Bundle 'tpope/vim-fugitive'
Bundle 'tpope/vim-surround'
Bundle 'tpope/vim-markdown'
Bundle 'yurifury/hexHighlight'
"Bundle 'bkad/CamelCaseMotion'
Bundle 'majutsushi/tagbar'
Bundle 'godlygeek/tabular'
"Bundle 'kien/ctrlp.vim'
Bundle 'myusuf3/numbers.vim'
Bundle 'othree/html5.vim'
"Bundle 'ChrisYip/Better-CSS-Syntax-for-Vim'
Bundle 'mattn/zencoding-vim'
Bundle 'willpragnell/vim-reprocessed'
Bundle 'pangloss/vim-javascript'
Bundle 'nvie/vim-flake8'
Bundle 'hynek/vim-python-pep8-indent'
"Bundle 'klen/python-mode'
Bundle 'davidhalter/jedi-vim'
Bundle 'jmcantrell/vim-virtualenv'
Bundle 'vim-scripts/javacomplete'
Bundle 'bling/vim-airline'
Bundle 'groenewege/vim-less'
Bundle 'Glench/Vim-Jinja2-Syntax'
Bundle 'chrisbra/NrrwRgn'

"Jedi
let g:jedi#popup_on_dot = 0
let g:jedi#popup_select_first = 0
"let g:jedi#auto_initialization = 0

"AirLine
let g:airline_theme='wombat'

"haskellmode stuff
au BufEnter *.hs compiler ghc
let g:haddock_browser = "/usr/bin/chromium"
let g:ghc = "/usr/bin/ghc"
let g:haddock_docdir = "/usr/share/doc/ghc/html/"

"EDITOR FUNCTIONALITY"
set fileformats=unix,dos
filetype indent on
filetype plugin on
set autochdir
set nobackup
set nowritebackup
set noswapfile
set history=500
set backspace=2
set relativenumber
set splitbelow

"Searching"
set hlsearch
set incsearch
set ignorecase
set smartcase

"Menus"
set completeopt=longest,menuone
set wildmenu
set wildmode=list:longest,full

"Completion"
set complete=.,w,b,u,t,i,]
inoremap <expr> <C-Space> pumvisible() \|\| &omnifunc == '' ?
\ "\<lt>C-n>" :
\ "\<lt>C-x>\<lt>C-o><c-r>=pumvisible() ?" .
\ "\"\\<lt>c-n>\\<lt>c-p>\\<lt>c-n>\" :" .
\ "\" \\<lt>bs>\\<lt>C-n>\"\<CR>"
imap <C-@> <C-Space>

"EDITOR APPEARANCE"
set visualbell
set number
set ruler
set shortmess=atI
set cursorline

if has('gui_running')
	if hostname() == 'ArchSoda'
		"set guifont=Source\ Code\ Pro\ 8

		"Powerline
		set guifont=PragmataPro\ 10 "Holy crap this font is amazing
		set laststatus=2
		set noshowmode
	else
		set guifont=Consolas:h10
	endif
	colorscheme bettong
	set guioptions-=T
	set guioptions-=m
	set guioptions-=r
	set colorcolumn=80

else
	colorscheme wombat256mod
	set laststatus=2
	set noshowmode
	set colorcolumn=80
endif

"Formatting"
syntax on
set autoindent
set smartindent
set shiftwidth=4
set tabstop=4

set nowrap
set sidescroll=5

set listchars=eol:$,tab:t-,trail:~,precedes:<,extends:>
set display+=lastline

"CUSTOM BINDS"

"Turn off highlighting with enter"
nnoremap <CR> :nohlsearch<CR>/<BS><CR>
"Faster Escape Bind"
inoremap jj <Esc>
"inoremap hh <Esc>

"Go to next/prev error
noremap <c-N> :lnext<CR>
noremap <c-P> :lprev<CR>

"NERDTree Toggle
noremap <Leader><Tab> :NERDTreeToggle<CR>

"TabBarToggle
noremap <Leader>1 :TagbarToggle<CR>

"Relative Numbers Toggle
noremap <Leader>n :NumbersToggle<CR>

"Faster window resizing"
"if bufwinnr(1)
"  map <c-n> <c-W><
"  map <c-m> <c-W>>
"endif

"CUSTOM COMMANDS"
"command! TrailingSpaces :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>
nnoremap <Leader>rtw :%s/\s\+$//e<cr>
command! SmallFont :set guifont=PragmataPro\ 9
"command! SmallFont :set guifont=Source\ Code\ Pro\ for\ Powerline\ Semi-Bold\ 7

"Automatic Closing Braces
inoremap {<CR> {<CR>}<Esc>O
inoremap {} {}

"Preserve undo history in ins mode
inoremap <c-u> <c-g>u<c-u>
inoremap <c-w> <c-g>u<c-w>
