"Enable Eclim"
function! StartEclim ()
	if (index(g:pathogen_disabled, 'eclim') >= 0)
		call remove(g:pathogen_disabled, 'eclim')
		call pathogen#infect()

		"let g:EclimBaseDir = '$VIM/vimfiles/bundle/eclim/eclim/'

		"Make Eclim use external Nailgun"
		let g:EclimNailgunClient = 'external'
		runtime! plugin/eclim.vim
	endif
endfunction

command! StartEclim call StartEclim()

setlocal omnifunc=eclim#java#complete#CodeComplete

setlocal foldmethod=indent
setlocal foldlevel=10

"Some convenient binds

noremap <Leader>j :JavaCorrect<CR>
noremap <Leader>i :JavaImport<CR>
noremap <Leader>v :Validate<CR>
